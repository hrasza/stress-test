#!/bin/bash



LOG_FILE=disk-log.txt
OP=$1
DISC_UTILIZATION=$2
PARTITION=$3
URL=$4
FILE=bigfile

display_usage() {
    echo "This script must be run without super-user privileges."
    echo -e "\nUsage:\n  ./disk.sh start 90 /dev/xvda1 http://localhost:8080\n"
    echo -e "\nUsage:\n  ./disk.sh stop\n"
}

# if less than 1 arguments supplied, display usage
    if [[ ( $# -le 0 ) ]]
    then
            display_usage
            exit 1
    fi

# check whether user had supplied -h or --help . If yes display usage
    if [[ ( $# == "--help") ||  $# == "-h" ]]
    then
            display_usage
            exit 0
    fi

# display usage if the script is not run as root user
    if [[ $USER == "root" ]]; then
            echo "This script must not run as root!"
            exit 1
    fi



if [[ $OP == "stop" ]]; then
# Check if the file exist then delete it
	if test -f "$FILE"; then
	    echo "$FILE exist"
	    rm ${FILE}
	    cat ${LOG_FILE}
	fi

else
#Empty log file
echo "">${LOG_FILE}

#Before test stats
echo "Before: ">>${LOG_FILE}
bash test.sh $URL >>${LOG_FILE}


LEFT=$(df -BM "$PARTITION" | tail -1 | awk '{print $3}'| sed 's/M//')
echo "Total disk space left ${LEFT}"

TOTAL=$(df -BM "$PARTITION" | tail -1 | awk '{print $2}'| sed 's/M//')
echo "Total disk space left ${TOTAL}"

#Calcluate space needed to fill specified percentage
SPACE_TO_FILL=$((((TOTAL/100) *DISC_UTILIZATION)-LEFT));

#fill the disk
fallocate -l ${SPACE_TO_FILL}M ${FILE}


#After test stats
echo "After: ">>${LOG_FILE}
bash test.sh $URL >>${LOG_FILE}

cat ${LOG_FILE}

fi