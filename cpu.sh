#!/bin/bash

OP=$1
LOG_FILE=cpu-log.txt
CORE=$2
URL=$3

display_usage() {
    echo "This script must be run without super-user privileges."
    echo -e "\nUsage:\n  ./cpu.sh start 1 http://localhost:8080\n"
    echo -e "\nUsage:\n  ./cpu.sh stop\n"
}

# if less than 1 arguments supplied, display usage
    if [[ ( $# -le 0 ) ]]
    then
            display_usage
            exit 1
    fi

# check whether user had supplied -h or --help . If yes display usage
    if [[ ( $# == "--help") ||  $# == "-h" ]]
    then
            display_usage
            exit 0
    fi

# display usage if the script is not run as root user
    if [[ $USER == "root" ]]; then
            echo "This script must not run as root!"
            exit 1
    fi

if [[ $OP == "stop" ]];
    then
    echo "Stopping stess process"
    pkill stress

    echo "************************STRESS CPU END****************************">>${LOG_FILE}

    #print log
    cat ${LOG_FILE}
else

#Check if pre-req is istalled or not
type stress >/dev/null 2>&1 || { echo >&2 "I require stress but it's not installed.  Aborting."; exit 1; }

echo "Total CPU core utilisation: ${CORE}"

#Logging the stats
echo "************************STRESS CPU START****************************">>${LOG_FILE}

#Empty log file
echo "">${LOG_FILE}

#Before test stats
echo "Before: ">>${LOG_FILE}
bash test.sh $URL >>${LOG_FILE}

# Stress using CPU-bound task
stress -c $CORE &

#After test stats
echo "After: ">>${LOG_FILE}
bash test.sh $URL >>${LOG_FILE}

cat ${LOG_FILE}

fi