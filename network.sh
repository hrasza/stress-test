#!/bin/bash

OP=$1
NETWORK_INTERFACE=$2
DELAY=$3
URL=$4
LOG_FILE=network-log.txt

display_usage() {
    echo "This script must be run with super-user privileges."
    echo -e "\nUsage:\n sudo ./network.sh start eth0 500 http://localhost:8080\n"
    echo -e "\nUsage:\n sudo  ./network.sh stop eth0\n"
}

# if less than 1 arguments supplied, display usage
    if [[ ( $# -le 1 ) ]]
    then
            display_usage
            exit 1
    fi

# check whether user had supplied -h or --help . If yes display usage
    if [[ ( $# == "--help") ||  $# == "-h" ]]
    then
            display_usage
            exit 0
    fi

# display usage if the script is not run as root user
    if [[ $USER != "root" ]]; then
            echo "This script must run as root!"
            exit 1
    fi

# Check if the file exist then delete it

echo $OP;

if [[ $OP == "stop" ]]; then
	echo "Deleting TC Delay..";
    sudo tc qdisc del dev ${NETWORK_INTERFACE} root
	cat ${LOG_FILE}
else

	#Empty log file
	echo "">${LOG_FILE}

	#Before test stats
	echo "Before: ">>${LOG_FILE}
	bash test.sh $URL >>${LOG_FILE}

	#RUN tc command for delay in network interface
	sudo tc qdisc add dev ${NETWORK_INTERFACE} root netem delay ${DELAY}ms

	#After test stats
	echo "After: ">>${LOG_FILE}
	bash test.sh $URL >>${LOG_FILE}

	cat ${LOG_FILE}

fi