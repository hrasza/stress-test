#!/bin/bash


## Maximim acceptable response time in sec
MAX_ACCEPTABLE_REPOSE_TIME=5

display_usage() {
        echo -e "\nUsage:\n  ./test.sh http://localhost:8080 \n"
}

# if less than two arguments supplied, display usage
        if [  $# -le 0 ]
        then
                display_usage
                exit 1
        fi

# check whether user had supplied -h or --help . If yes display usage
        if [[ ( $# == "--help") ||  $# == "-h" ]]
        then
                display_usage
                exit 0
        fi

URL=$1

echo `curl -L -s -w '%{http_code}' -o /dev/null $1`

HTTP_STATUS=$(curl -L -s -w '%{http_code}' -o /dev/null $URL)
RESPONSE_TIME=$(curl -L -s -w '%{time_total}' -o /dev/null $URL)

echo $HTTP_STATUS;
echo $RESPONSE_TIME;

if [[ ${HTTP_STATUS} == 200 && ${RESPONSE_TIME} < ${MAX_ACCEPTABLE_REPOSE_TIME} ]]; then
        echo "Response time OK"
else
        echo "Response time exceeded, test failed"
fi